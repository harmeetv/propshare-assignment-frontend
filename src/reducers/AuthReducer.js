import {
	LOGIN_START,
	LOGIN_SUCCESS,
	LOGIN_FAIL,
	SIGNUP_START,
	SIGNUP_SUCCESS,
	SIGNUP_FAIL,
	LOGOUT
} from '../actions/types';

const INITIAL_STATE = {
	loading: false,
	token: null,
	logInError: '',
	signUpError: ''
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case LOGIN_START:
			return { ...state, logInError: '', loading: true }
		case LOGIN_SUCCESS:
			return { ...state, logInError: '', token: action.payload.token, loading: false }
		case LOGIN_FAIL:
			return { ...state, logInError: action.payload, loading: false }
		case SIGNUP_START:
			return { ...state, signUpError: '', loading: true }
		case SIGNUP_SUCCESS:
			return { ...state, signUpError: '', token: action.payload.token, loading: false }
		case SIGNUP_FAIL:
			return { ...state, signUpError: action.payload, loading: false }
		case LOGOUT:
			return { ...state, token: null }
		default:
			return state;
	}
};
