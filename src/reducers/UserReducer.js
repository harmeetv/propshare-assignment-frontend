import {
	UPDATE_PROFILE_START,
	UPDATE_PROFILE_SUCCESS,
	UPDATE_PROFILE_FAIL,
	GET_PROFILE_START,
	GET_PROFILE_SUCCESS,
	GET_PROFILE_FAIL,
	TOGGLE_DRAWER,
	SET_UPDATE_PROFILE_FIELD,
	GET_HOME_START,
	GET_HOME_SUCCESS,
	GET_HOME_FAIL
} from '../actions/types';

const INITIAL_STATE = {
	loading: false,
	firstName: "",
	lastName: "",
	dob: "",
	gender: "",
	isDrawerOpen: false,
	homeData: null
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case UPDATE_PROFILE_START:
			return { ...state, loading: true }
		case UPDATE_PROFILE_SUCCESS:
			return { ...state, loading: false }
		case UPDATE_PROFILE_FAIL:
			return { ...state, loading: false }
		case GET_PROFILE_START:
			return { ...state, loading: true }
		case GET_PROFILE_SUCCESS:
			return { ...state, firstName: action.payload.firstName, lastName: action.payload.lastName, gender: action.payload.gender, dob: action.payload.dob, loading: false }
		case GET_PROFILE_FAIL:
			return { ...state, loading: false }
		case TOGGLE_DRAWER:
			return { ...state, isDrawerOpen: !state.isDrawerOpen }
		case SET_UPDATE_PROFILE_FIELD:
			return { ...state, [action.payload.field]: action.payload.value }
		case GET_HOME_START:
			return { ...state, loading: true }
		case GET_HOME_SUCCESS:
			return { ...state, homeData: action.payload, loading: false }
		case GET_HOME_FAIL:
			return { ...state, loading: false }
		default:
			return state;
	}
};
