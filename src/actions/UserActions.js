import axios from 'axios';
import { apiBaseUrl } from '../config';

import {
	UPDATE_PROFILE_START,
	UPDATE_PROFILE_SUCCESS,
	UPDATE_PROFILE_FAIL,
	GET_PROFILE_START,
	GET_PROFILE_SUCCESS,
	GET_PROFILE_FAIL,
	TOGGLE_DRAWER,
	SET_UPDATE_PROFILE_FIELD,
	GET_HOME_START,
	GET_HOME_SUCCESS,
	GET_HOME_FAIL
} from './types';

export const updateProfile = (firstName, lastName, gender, dob, push) => {
	return dispatch => {
		dispatch({
    		type: UPDATE_PROFILE_START
    	});
		axios({
	        method: 'post',
	        url: '/updateProfile',
	        data: {
	            firstName,
	            lastName,
	            gender,
	            dob
	        }
	    })
	    .then(response => {
	    	if (response.data.success === true) {
	    		dispatch({
	    			type: UPDATE_PROFILE_SUCCESS
	    		});
	    		push("/home");
	    	} else {
	    		dispatch({
	                type: UPDATE_PROFILE_FAIL,
	                payload: "Something went wrong"
	            });
	    	}
	    })
	    .catch(error => {
	    	dispatch({
	            type: UPDATE_PROFILE_FAIL,
	            payload: "Something went wrong"
	        });
	    });
	}
}

export const getProfile = () => {
	return dispatch => {
		dispatch({
    		type: GET_PROFILE_START
    	});
		axios({
	        method: 'get',
	        url: '/getProfile'
	    })
	    .then(response => {
	    	if (response.data.success === true) {
	    		dispatch({
	    			type: GET_PROFILE_SUCCESS,
	    			payload: response.data
	    		});
	    	} else {
	    		dispatch({
	                type: GET_PROFILE_FAIL,
	                payload: "Something went wrong"
	            });
	    	}
	    })
	    .catch(error => {
	    	dispatch({
	            type: GET_PROFILE_FAIL,
	            payload: "Something went wrong"
	        });
	    });
	}
}

export const toggleDrawer = () => {
	return {
		type: TOGGLE_DRAWER
	}
}

export const setUpdateProfileFields = (field, value) => {
	return {
		type: SET_UPDATE_PROFILE_FIELD,
		payload: {
			field,
			value
		}
	}
}

export const getHome = () => {
	return dispatch => {
		dispatch({
    		type: GET_HOME_START
    	});
		axios({
	        method: 'get',
	        url: '/home'
	    })
	    .then(response => {
	    	if (response.data.success === true) {
	    		dispatch({
	    			type: GET_HOME_SUCCESS,
	    			payload: response.data
	    		});
	    	} else {
	    		dispatch({
	                type: GET_HOME_FAIL,
	                payload: "Something went wrong"
	            });
	    	}
	    })
	    .catch(error => {
	    	dispatch({
	            type: GET_HOME_FAIL,
	            payload: "Something went wrong"
	        });
	    });
	}
}
