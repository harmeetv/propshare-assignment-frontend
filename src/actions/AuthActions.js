import axios from 'axios';
import JwtDecode from 'jwt-decode';
import { apiBaseUrl } from '../config';

import {
	LOGIN_START,
	LOGIN_SUCCESS,
	LOGIN_FAIL,
	SIGNUP_START,
	SIGNUP_SUCCESS,
	SIGNUP_FAIL,
	LOGOUT
} from './types';

export const login = (username, password, push) => {
	return dispatch => {
		dispatch({
    		type: LOGIN_START
    	});
		axios({
	        method: 'post',
	        url: '/login',
	        data: {
	            username,
	            password
	        }
	    })
	    .then(response => {
	    	if (response.data.success === true) {
	    		dispatch({
	    			type: LOGIN_SUCCESS,
	    			payload: {
	    				token: response.data.token
	    			}
	    		});
	    		localStorage.setItem('token', response.data.token);
	    		axios.defaults.headers.common['Authorization'] = `Bearer ${response.data.token}`;
	    		push(response.data.nextPage);
	    	} else {
	    		dispatch({
	                type: LOGIN_FAIL,
	                payload: "Wrong username/password combination"
	            });
	    	}
	    })
	    .catch(error => {
	    	dispatch({
	            type: LOGIN_FAIL,
	            payload: "Wrong username/password combination"
	        });
	    });
	}
}

export const signup = (username, password, confirmPassword, push) => {
	return dispatch => {
		dispatch({
    		type: SIGNUP_START
    	});
		axios({
	        method: 'post',
	        url: '/signup',
	        data: {
	            username,
	            password,
	            confirmPassword
	        }
	    })
	    .then(response => {
	    	if (response.data.success === true) {
	    		dispatch({
	    			type: SIGNUP_SUCCESS,
	    			payload: {
	    				token: response.data.token
	    			}
	    		});
	    		localStorage.setItem('token', response.data.token);
	    		axios.defaults.headers.common['Authorization'] = `Bearer ${response.data.token}`;
	    		push(response.data.nextPage);
	    	} else {
	    		dispatch({
	                type: SIGNUP_FAIL,
	                payload: response.data.message
	            });
	    	}
	    })
	    .catch(error => {
	    	dispatch({
	            type: SIGNUP_FAIL,
	            payload: error.response.data.message
	        });
	    });
	}
}

export const autoLogin = () => {
    let token = null;
    token = localStorage.getItem('token');
    return (dispatch) => {
        if (token != null) {
        	axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
            let decodeData = JwtDecode(token);
            let now = (new Date()).getTime();
            if (decodeData.exp*1000 > now) {
                dispatch({
                    type: LOGIN_SUCCESS,
                    payload: {
                        token
                    }
                });
            }
            else {
                localStorage.removeItem('token');
                dispatch({
                    type: LOGOUT
                });
            }
        }
    }
}

export const logout = () => {
	return (dispatch) => {
        localStorage.removeItem('token');
        dispatch({
            type: LOGOUT
        });
    }
}
