import React from 'react';

let LoginScreen = React.lazy(() => import(`./screens/LoginScreen/LoginScreen`));
let UpdateProfileScreen = React.lazy(() => import(`./screens/UpdateProfileScreen/UpdateProfileScreen`));
let ListingScreen = React.lazy(() => import(`./screens/ListingScreen/ListingScreen`));

let routes = [
	{
		component: LoginScreen,
		path: '/',
		exact: true
	},
	{
		component: UpdateProfileScreen,
		path: '/updateProfile',
		exact: true
	},
	{
		component: ListingScreen,
		path: '/home',
		exact: true
	}	
];

export default routes;
