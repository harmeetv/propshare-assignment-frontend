import React, { Component } from 'react';
import { Menu, MoreVert, Add } from '@material-ui/icons';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Fab from '@material-ui/core/Fab';
import Button from '@material-ui/core/Button';

import Post from '../../components/Post/Post';
import SideMenu from '../../components/SideMenu/SideMenu';
import { toggleDrawer, getHome } from '../../actions';


class ListingScreen extends Component {
	componentDidMount() {
		if (this.props.token===null) {
			this.props.history.push('/');
		}
		else {
			this.props.getHome();
		}
	}
	renderPosts(item, index) {
		return <Post key={index} title={item.title} image={item.media} />
	}
	render() {
		return (
			<>
				<div className="ListingScreenContainer">
					<div className="header">
						<Button onClick={this.props.toggleDrawer}><Menu className="headerIcons" /></Button>
						{this.props.homeData!==null && <div className="flex1 headerTitle">{this.props.homeData.firstName} {this.props.homeData.lastName}</div>}
						<Button><MoreVert className="headerIcons" /></Button>
					</div>
					<div className="listContainer">
						{this.props.homeData!==null && this.props.homeData.posts.map(this.renderPosts)}
					</div>
				</div>
				<Fab className="fab" color="primary" aria-label="add">
					<Add />
				</Fab>
				<SideMenu />
			</>
		);
	}
}

const mapStateToProps = ({ auth, user }) => {
  const { token } = auth;
  const { homeData } = user;
  return { token, homeData };
}

export default withRouter(connect(mapStateToProps, { toggleDrawer, getHome })(ListingScreen));
