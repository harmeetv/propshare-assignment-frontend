import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import InputField from '../../components/InputField/InputField';
import { login, signup } from '../../actions';

class LoginScreen extends Component {
	state = {
		view: "login",
		loginId: "",
		loginPassword: "",
		confirmPassword: "",
		error: ""
	}
	componentDidMount() {
		if (this.props.token!==null) {
			this.props.history.push('/home');
		}
	}
	onLoginIdChange(event) {
		this.setState({loginId: event.target.value});
	}
	onLoginPasswordChange(event) {
		this.setState({loginPassword: event.target.value});
	}
	onConfirmPasswordChange(event) {
		this.setState({confirmPassword: event.target.value});
	}
	onLoginButtonPress() {
		if (this.state.view==="login" && this.state.loginId!=="" && this.state.loginPassword!=="") {
			this.props.login(this.state.loginId, this.state.loginPassword, this.props.history.push);
		}
		else if (this.state.view==="signup" && this.state.loginId!=="" && this.state.loginPassword!=="" && this.state.confirmPassword!=="") {
			this.props.signup(this.state.loginId, this.state.loginPassword, this.state.confirmPassword, this.props.history.push);
		}
	}
	toggleView() {
		this.setState(prevState => {
			return {...prevState, view: (prevState.view==="login"?"signup":"login")}
		});
	}
	render() {
		return (
			<div className="loginScreenContainer">
				<div className="loginScreenHeader">
					<div>PWA</div>
					<div>Catchphrase</div>
				</div>
				<div className="loginForm">
					<InputField value={this.state.loginId} onChange={this.onLoginIdChange.bind(this)} className="loginScreenInputField" type="text" placeholder="Login Id" />
					<InputField value={this.state.loginPassword} onChange={this.onLoginPasswordChange.bind(this)} className="loginScreenInputField" type="password" placeholder="Password" />
					{this.state.view==="signup" && <InputField value={this.state.confirmPassword} onChange={this.onConfirmPasswordChange.bind(this)} className="loginScreenInputField" type="password" placeholder="Confirm Password" />}
					<Button onClick={this.onLoginButtonPress.bind(this)} className="loginScreenInputButton inputButton" variant="contained">{this.state.view==="login" ? "Log In" : "Sign Up"}</Button>
					{this.state.view==="login" && this.props.logInError!=="" && <div className="loginError">{this.props.logInError}</div>}
					{this.state.view==="signup" && this.props.signUpError!=="" && <div className="loginError">{this.props.signUpError}</div>}
					<div className="toggleLoginSignupView" onClick={this.toggleView.bind(this)}>{this.state.view==="login" ? "Create New Account" : "Login to Existing Account"}</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = ({ auth }) => {
  const { token, logInError, signUpError } = auth;
  return { token, logInError, signUpError };
}

export default withRouter(connect(mapStateToProps, { login, signup })(LoginScreen));
