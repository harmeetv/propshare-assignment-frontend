import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import InputField from '../../components/InputField/InputField';
import { updateProfile, getProfile, setUpdateProfileFields } from '../../actions';

class UpdateProfileScreen extends Component {
	componentDidMount() {
		if (this.props.token===null) {
			this.props.history.push('/');
		}
		else {
			this.props.getProfile();
		}
	}
	setGender(gender) {
		this.props.setUpdateProfileFields("gender", gender);
	}
	onFirstNameChange(event) {
		this.props.setUpdateProfileFields("firstName", event.target.value);
	}
	onLastNameChange(event) {
		this.props.setUpdateProfileFields("lastName", event.target.value);
	}
	onDobChange(event) {
		this.props.setUpdateProfileFields("dob", event.target.value);
	}
	onUpdateButtonPress() {
		this.props.updateProfile(this.props.firstName, this.props.lastName, this.props.gender, this.props.dob, this.props.history.push);
	}
	render() {
		return (
			<div className="updateProfileScreenContainer">
				<div className="section">
					<div className="sectionHeader">What's your name?</div>
					<InputField value={this.props.firstName} onChange={this.onFirstNameChange.bind(this)} type="text" placeholder="First Name" />
					<InputField value={this.props.lastName} onChange={this.onLastNameChange.bind(this)} type="text" placeholder="Last Name" />
				</div>
				<div className="section">
					<div className="sectionHeader">And your gender?</div>
					<div className="genderSelector">
						<div onClick={() => this.setGender("f")} className={this.props.gender==="f" ? "activeGender": "inActiveGender"}>Female</div>
						<div onClick={() => this.setGender("m")} className={this.props.gender==="m" ? "activeGender": "inActiveGender"}>Male</div>
					</div>
				</div>
				<div className="section">
					<div className="sectionHeader">
						What's your date of birth?
						<div className="sectionHeaderHint">This can't be changed later</div>
					</div>
					<InputField onChange={this.onDobChange.bind(this)} value={this.props.dob} type="date" placeholder="First Name" max={new Date().toISOString().slice(0,10)} />
				</div>
				<div className="flex1" />
				<div className="section">
					<div onClick={this.onUpdateButtonPress.bind(this)} className="updateProfileButton">›</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = ({ auth, user }) => {
  const { token } = auth;
  const { firstName, lastName, gender, dob } = user;
  return { token, firstName, lastName, gender, dob };
}

export default withRouter(connect(mapStateToProps, { updateProfile, getProfile, setUpdateProfileFields })(UpdateProfileScreen));
