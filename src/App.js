import React, { Component, Suspense } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { autoLogin } from './actions';
import Routes from './Routes';
import './App.css';

class App extends Component {
	componentDidMount() {
		this.props.autoLogin();
	}
	render() {
		console.log(this.props);
		return (
			<>
			<Suspense fallback={"Loading..."}>
				{renderRoutes(Routes)}
			</Suspense>
			<Backdrop className="backdrop" open={this.props.authLoading || this.props.userLoading}>
				<CircularProgress className="backdropCircle" />
			</Backdrop>
			</>
		);
	}
}

const mapStateToProps = ({ auth, user }) => {
  return { authLoading: auth.loading, userLoading: user.loading };
}


export default withRouter(connect(mapStateToProps, { autoLogin })(App));
