import React, { Component } from 'react';
import { Home, HomeWork, Apartment, Domain, ExitToApp } from '@material-ui/icons';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import { logout, toggleDrawer } from '../../actions';

class SideMenu extends Component {
	logout() {
		this.props.logout();
		this.props.history.push("/");
	}
	render() {
		return (
			<Drawer open={this.props.isDrawerOpen} onClose={this.props.toggleDrawer}>
				<div className="drawerContainer">
					<div className="drawerHeader">PROPSHARE</div>
				</div>
				<List component="nav" aria-label="main mailbox folders">
					<ListItem button>
						<ListItemIcon>
							<Home />
						</ListItemIcon>
						<ListItemText primary="Home" />
					</ListItem>
					<Divider/>
					<ListItem button>
						<ListItemIcon>
							<HomeWork />
						</ListItemIcon>
						<ListItemText primary="Properties" />
					</ListItem>
					<ListItem button>
						<ListItemIcon>
							<Apartment />
						</ListItemIcon>
						<ListItemText primary="Apartments" />
					</ListItem>
					<ListItem button>
						<ListItemIcon>
							<Domain />
						</ListItemIcon>
						<ListItemText primary="Flats" />
					</ListItem>
					<Divider />
					<ListItem button onClick={this.logout.bind(this)}>
						<ListItemIcon>
							<ExitToApp />
						</ListItemIcon>
						<ListItemText primary="Log Out" />
					</ListItem>
				</List>
			</Drawer>
		);
	}
}

const mapStateToProps = ({ user }) => {
  const { isDrawerOpen } = user;
  return { isDrawerOpen };
}

export default withRouter(connect(mapStateToProps, { logout, toggleDrawer })(SideMenu));
