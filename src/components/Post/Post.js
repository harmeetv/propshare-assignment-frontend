import React, { Component } from 'react';

import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

class Post extends Component {
	render() {
		return (
			<Card className="postContainer" variant="outlined">
				<CardHeader
					className="postHeader"
					title={this.props.title}
				/>
				<CardMedia
					image={this.props.image}
					title={this.props.title}
					className="postImage"
				/>
				<CardContent>
					<Typography variant="body2" color="textSecondary" component="p">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					</Typography>
				</CardContent>
			</Card>
		);
	}
}

export default Post;
