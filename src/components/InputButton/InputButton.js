import React from 'react';

function InputButton(props) {
	return <input type="button" className={["inputButton", props.className].join(" ")} value={props.children} />;
}

export default InputButton;
