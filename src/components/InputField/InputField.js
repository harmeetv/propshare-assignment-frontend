import React from 'react';

function InputField(props) {
	return <input value={props.value} onChange={props.onChange} className={["inputField", props.className].join(" ")} type={props.type} placeholder={props.placeholder} max={props.max} />;
}

export default InputField;
